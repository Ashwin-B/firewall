# Firewall

## Introduction of Firewall

Firewalls are used to prevent the unauthorized or unauthenticated access to our network from outside world. It can be implemented on both hardware and software, or combination of both. All data entering or leaving the intranet pass through the firewall which examines each packet and blocks that do not meet the specified security criteria. Theoritically, there are two types of firewall:

1. Network Layer
2. Application Layer

### Network Layer

Network layer firewalls make decisions based on the source address, destination address and ports in individual IP Packets. One of the most important difference about many network layer firewalls is that they route traffic directly through them. If the user wants to use one, they need to have a validly assigned IP Address block or private internet address block. The network firewall is _fast_ and almost _transparent_ to its users.

### Application Layer

Application firewalls are hosts running proxy servers, which permit no traffic directly between networks and perform elaborate logging and examination of traffic. Sometimes, the application firewall impact the performance and become less transparent to the end users. In the early days, the application firewall is less transparent and it is difficult for the end-users. However, the modern application firewall is totally transparent. It also provides detailed information about audit reports and tends to enforce advanced security models than network layer firewalls.

## Working of Firewall

When a network traffic is being received by the network, the firewall will match the network traffic against a set of rules. Once the rules are matched, associate actions are applied to the network traffic. Rules can be defined on the firewall based on the necessity and security policies of the organisation. For example, HR Department cannot access the code server and at the same time another rule can also be defined such as the administrator can access both HR and technical server. Firewall will have distinct set of rules for both incoming and outgoing traffic. Mostly the outgoing traffic originated from the server is allowed to pass because setting a rule to the outgoing traffic is always better inorder to achieve more security and prevent unwanted communication. Firewall treats the incoming traffic differently. There are three major protocol layers which are TCP, UDP and ICMP. The incoming traffic will have any of these three protocol layers. All these protocol layers will have source address and destination address. TCP and UDP will have _port numbers_ whereas ICMP will have _type code_ which is used to identify the purpose of the packet. It is very difficult to cover every possible rule explicitly. So every firewall will have default policy. Whenever a particular set of rules isn,t being defined by the user. The firewall uses default policy. For example, if a rule is not being defined about SSH connection, then the firewall will use the default policy to manage that connection. The default policy consists of actions such as accept and drop. If the default policy is set to accept, then it will accept all SSH connection from outside. Therefore setting the default policy to drop is a very good practice.

## Future of Firewall

The future of firewalls exist somewhere in between both application layer firewall and network layer firewall. It is likely that the the network layer firewall will become increasingly aware of the information passing through them and the application layer will become more and more transparent.
